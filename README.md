# HW1_VK

# Отчет

## Постановка задачи
Проект по курсу "Семинар наставника" был предназначен для парсинга данных пользователей из социальной сети VK, для обработки данных с помощью Python и визуализации их в DataLens, а также тестирование.

Согласно варианту работа велась с пользователями из города Тюмень.


## Содержание проекта

### HW1_VK.ipynb
Ноутбук, содержащий основной код по работе с пользователями:
- Импорт всех необходимых библиотек;
- Подключение к API VK;
- Вывод информации о себе;
- Выгрузка данных;
- Сохранение датафрейма в csv формат;
- Преобразование датафрейма с помощью функций;
- Тест функций для преобразований;
- Вывод топ 5 имен;


### function.py
Файл содержит функции для работы с датафреймом: 
- drop_columns - удаляет определенные столбцы из датафрейма;
- remane_columns_to_russian - переименовывает столбцы;


### test_my_code.py
Файл содержит тест функции для теста основных функций:
- test_drop_columns - тестируется функция drop_columns;
- test_rename_columns - тестируется функция remane_columns_to_russian;


### .gitignore

## Используемые технологии

Список используемых технологий:
- IDE Microsoft Visual Studio Code
- Python, JupyterNotebook
- API VK documentation
- Yandex.DataLens
- Git, GitLab
- Markdown

## Итоги

-Изучила  с Git, GitLab
-Через API VK выгрузила набор данных
-Результат представила в [дашборде DataLens](https://datalens.yandex.ru/9wtrnshg8easy-dashbordvk?state=421cbb3a187)



## Выводы по тестированию
 Я создала две функции и для каждой написала тест
 
---------- coverage: platform darwin, python 3.11.4-final-0 ----------

Name              Stmts   Miss  Cove

function.py           7      0   100%

test_my_code.py      13      0   100%


TOTAL                20      0   100%



## Ссылки на используемые источники
- https://git-scm.com/book/ru/v2
- https://dev.vk.com/ru/reference
- https://cloud.yandex.ru/docs/datalens/
- https://pypi.org/project/pytest-cov/
- https://pypi.org/project/httpretty/
- https://losst.pro/kak-polzovatsya-gitlab?ysclid=lq79ijrgsj581110947



