
import pandas as pd
from function import drop_columns
from function import rename_columns_to_russian

def test_drop_columns():
    df = pd.read_csv("tumen_users3.csv") 
    assert "is_closed" in df.columns
    result_df = drop_columns(df)
    assert "is_closed" not in result_df.columns
    # expected_columns =  ['id',] 
    # assert list(result_df.columns) == expected_columns

def test_rename_columns():
    df = pd.read_csv("tumen_users3.csv") 
    assert "sex" in df.columns
    result_df = rename_columns_to_russian(df)
    
    assert "Пол"  in result_df.columns

